Integration [grifix/entity-manager](https://packagist.org/packages/grifix/entity-manager) with [Symfony](https://symfony.com/)

# Installation

`composer require grifix/entity-manager-bundle`

# Usage

- Read the [Grifix Entity Manager documentation](https://gitlab.com/grifix/entity-manager/-/blob/1.0/README.md)
- set up configuration as in this example:

```yaml
  doctrine:
   dbal:
     dbname: '%env(resolve:POSTGRES_DB)%'
     host: db
     port: 5432
     user: '%env(resolve:POSTGRES_USER)%'
     password: '%env(resolve:POSTGRES_PASSWORD)%'
     driver: pdo_pgsql
  
  grifix_entity_manager:
   entities:
     - name: person
       table: persons
       object_class: Grifix\EntityManagerBundle\Tests\Dummies\Person
       version_converter: Grifix\EntityManagerBundle\Tests\Dummies\PersonVersionConverter
       dependencies:
         - outside
       schemas:
         #v1
         - - property: id
             type: string
           - property: name
             type: string
  
         #v2
         - - property: id
             type: string
           - property: firstName
             type: string
           - property: lastName
             type: string
  
  services:
   Grifix\EntityManagerBundle\Tests\Dummies\PersonVersionConverter:
     public: true
  
   Grifix\EntityManagerBundle\Tests\Dummies\PersonOutside:
     public: true
```

- Inject entity manager as a dependency or get it from
  the [Symfony Container](https://symfony.com/doc/current/service_container.html)
  instead of creating it by `EntityManager::create()`
