<?php

declare(strict_types=1);

namespace Grifix\EntityManagerBundle\Tests\Dummies;

final class Person
{
    public function __construct(
        public string $id,
        public string $firstName,
        public string $lastName,
        public readonly PersonOutside $outside
    ) {
    }
}
