<?php

declare(strict_types=1);

namespace Grifix\EntityManagerBundle\Tests\Dummies;

use Grifix\Normalizer\VersionConverter\Exceptions\UnsupportedVersionException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

final class PersonVersionConverter implements VersionConverterInterface
{

    public function convert(array $data, int $dataVersion, string $normalizerName): array
    {
        return match ($dataVersion) {
            1 => $this->convertToVersion2($data),
            default => throw new UnsupportedVersionException(
                $normalizerName,
                $dataVersion
            )
        };
    }

    private function convertToVersion2(array $data): array
    {
        $name = explode(' ', $data['name']);
        $data['firstName'] = $name[0];
        $data['lastName'] = $name[1];
        unset($data['name']);
        return $data;
    }
}
