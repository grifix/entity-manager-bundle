<?php

declare(strict_types=1);

namespace Grifix\EntityManagerBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\DBAL\Connection;
use Grifix\EntityManager\EntityManagerInterface;
use Grifix\EntityManagerBundle\GrifixEntityManagerBundle;
use Grifix\EntityManagerBundle\Tests\Dummies\Person;
use Grifix\EntityManagerBundle\Tests\Dummies\PersonOutside;
use Grifix\EntityManagerBundle\Tests\Dummies\UserCreatedEvent;
use Grifix\EventStore\Cli\PauseSubscriptionCommand;
use Grifix\EventStore\Cli\RunEventConsumerCommand;
use Grifix\EventStore\Cli\RunEventPublisherWorkerCommand;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventStore;
use Grifix\EventStore\EventStoreInterface;
use Grifix\NormalizerBundle\GrifixNormalizerBundle;
use Grifix\Uuid\Uuid;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class EventStoreBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(DoctrineBundle::class);
        $kernel->addTestBundle(GrifixEntityManagerBundle::class);
        $kernel->addTestBundle(GrifixNormalizerBundle::class);
        $kernel->addTestConfig(__DIR__ . '/config.yaml');
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $kernel->getContainer()->get(EntityManagerInterface::class);
        /** @var Connection $connection */
        $connection = $kernel->getContainer()->get('doctrine')->getConnection();
        $sql        = <<<SQL
create table if not exists persons
(
    id      uuid
        constraint persons_pk
            primary key,
    version bigint not null,
    data    jsonb  not null
);
SQL;

        $connection->executeQuery($sql);
        $connection->executeQuery('truncate table persons');
        $person = new Person(
            'f8fc4bbc-7957-45e9-9ed3-18e7fd299bc5',
            'John',
            'Connor',
            $kernel->getContainer()->get(PersonOutside::class)
        );
        $entityManager->add($person, 'f8fc4bbc-7957-45e9-9ed3-18e7fd299bc5');
        $entityManager->flush();
        self::assertEquals($person, $entityManager->get(Person::class, 'f8fc4bbc-7957-45e9-9ed3-18e7fd299bc5'));

        $connection->insert('persons', [
            'id'      => 'eafb50ed-b5ff-4bb7-905b-a15dca29303a',
            'version' => 1,
            'data'    => json_encode([
                'id'             => 'eafb50ed-b5ff-4bb7-905b-a15dca29303a',
                'name'           => 'John Connor',
                '__normalizer__' => [
                    'name'    => 'person',
                    'version' => 1
                ]
            ])
        ]);
        self::assertEquals(
            new Person(
                'eafb50ed-b5ff-4bb7-905b-a15dca29303a',
                'John',
                'Connor',
                new PersonOutside()
            ),
            $entityManager->get(Person::class, 'eafb50ed-b5ff-4bb7-905b-a15dca29303a')
        );
    }
}
