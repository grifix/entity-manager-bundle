<?php

declare(strict_types=1);

namespace Grifix\EntityManagerBundle;

use Grifix\EntityManager\EntityManager as BaseEntityManager;
use Grifix\EntityManager\EntityRepository\EntityRepository;
use Grifix\EntityManager\EntityTypeRegistry\EntityTypeRegistry;
use Grifix\Normalizer\DependencyProvider\DependencyProviderInterface;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;

final class EntityManager extends BaseEntityManager
{
    public function __construct(
        NormalizerInterface $normalizer,
        EntityTypeRegistry $entityTypeRegistry,
        EntityRepository $entityRepository,
        private readonly DependencyProviderInterface $dependencyProvider,
        array $entityConfigs,
    ) {
        parent::__construct($normalizer, $entityTypeRegistry, $entityRepository);
        foreach ($entityConfigs as $config) {
            $this->registerEntityConfig($config);
        }
    }

    private function registerEntityConfig(array $config): void
    {
        $versionConverter = null;
        if ( ! empty($config['version_converter'])) {
            $versionConverter = $this->dependencyProvider->get($config['version_converter']);
        }
        $this->registerEntityType(
            $config['name'],
            $config['object_class'],
            $config['table'],
            $this->createSchemas($config['schemas']),
            $versionConverter,
            $config['dependencies'] ?? []
        );
    }

    /**
     *
     * @return Schema[]
     */
    private function createSchemas(array $schemaConfigs): array
    {
        $result = [];
        foreach ($schemaConfigs as $schemaConfig) {
            $schema = Schema::create();
            $this->addPropertyToSchema($schema, $schemaConfig);
            $result[] = $schema;
        }

        return $result;
    }

    private function addPropertyToSchema(Schema $schema, array $schemaConfigs): void
    {
        foreach ($schemaConfigs as $propertyConfig) {
            match ($propertyConfig['type']) {
                'object' => $schema->withObjectProperty(
                    $propertyConfig['property'],
                    $propertyConfig['allowed_normalizers'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'array_of_objects' => $schema->withArrayOfObjectsProperty(
                    $propertyConfig['property'],
                    $propertyConfig['allowed_normalizers'],
                ),
                'string' => $schema->withStringProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'integer' => $schema->withIntegerProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'number' => $schema->withNumberProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'array' => $schema->withArrayProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'boolean' => $schema->withBooleanProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'mixed_object' => $schema->withMixedObjectProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                )
            };
        }
    }
}
